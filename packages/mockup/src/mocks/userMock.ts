import { Mocks } from '../utils/Mocks';
import { User } from '../typings';

export const userMock = new Mocks<Partial<User>>([
    {
        fullName: 'John Doe',
    },
    {
        fullName: 'Anna Smith',
    },
    {
        fullName: 'Patrick Stuart',
    },
    {
        fullName: 'Robert Jones',
    },
]);
