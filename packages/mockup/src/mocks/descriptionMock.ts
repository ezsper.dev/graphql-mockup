import { Mocks } from '../utils/Mocks';

export const descriptionMock = new Mocks<string>([
    'It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout.',
    'Desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for \'lorem ipsum\'',
]);
