import { Mocks } from '../utils/Mocks';
import { Product } from '../typings';

export const productMock = new Mocks<Partial<Product>>([
    {
        description: 'It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout.',
        subheading: 'Hamburguer de Carne',
        imageUrl: 'http://cdn.example.com/image/r3i12k72/2842g.jpg',
    },
    {
        description: 'It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout.',
        subheading: 'Hamburguer de Frango',
        imageUrl: 'http://cdn.example.com/image/r3i12k72/2842g.jpg',
    },
    {
        description: 'It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout.',
        subheading: 'Hamburguer de Peixe',
        imageUrl: 'http://cdn.example.com/image/r3i12k72/2842g.jpg',
    },
    {
        description: 'It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout.',
        subheading: 'Hamburguer Vegano',
        imageUrl: 'http://cdn.example.com/image/r3i12k72/2842g.jpg',
    },
]);
