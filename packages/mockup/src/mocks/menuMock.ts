import { Mocks } from '../utils/Mocks';
import { Menu } from '../typings';

export const menuMock = new Mocks<Partial<Menu>>([
    {
        id: '1'
    },
]);
