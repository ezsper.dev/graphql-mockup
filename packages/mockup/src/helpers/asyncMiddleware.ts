import { Application, NextFunction, RequestHandler, ErrorRequestHandler, Router } from 'express';

export const asyncMiddleware = (app: Application) => {
  const asyncHandler = (middleware: Router | RequestHandler | ErrorRequestHandler) => {
    if (middleware instanceof Router) {
      return injectMethods(middleware as any);
    }
    const handler = (...args: any[]) => {
      const next = args[args.length - 1] as NextFunction;
      const result = (middleware as any)(...args);
      if (result && 'then' in result) {
        result.then(() => {}, (error: Error) => {
          next(error);
        });
      }
      return result;
    };
    Object.defineProperty(handler, 'length', {
      configurable: true,
      value: middleware.length,
    });
    return handler;
  };

  const injectMethods = (target: Application | Router) => {
    for (const method of [
      'use' as 'use',
      'get' as 'get',
      'post' as 'post',
      'delete' as 'delete',
      'head' as 'head',
    ]) {
      const handler = app[method];
      target[method] = (...args: any[]) => {
        let i = method === 'use' && typeof args[0] === 'function' ? 0 : 1;
        for (; i < args.length; i++) {
          args[i] = Array.isArray(args[i])
              ? args[i].map((middleware: any) => asyncHandler(middleware))
              : asyncHandler(args[i]);
        }
        return handler.apply(app, args as any);
      };
    }
    return target;
  };

  return injectMethods(app);
};