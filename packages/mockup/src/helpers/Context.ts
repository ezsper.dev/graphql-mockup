import { Request } from 'express';
import { ExpressContext } from 'apollo-server-express/dist/ApolloServer';
import { ContextFunction } from 'apollo-server-core';

type SimpleRequestContext = {
  req: Request;
};

export const createContext: ContextFunction<ExpressContext, Context> = (integration) => {
  (integration.req as any).ctx = integration;
  return integration;
};

export const getContextFromRequest = (req: Request): Context => {
  if ((req as any).ctx) {
    return (req as any).ctx;
  }
  const ctx = { req };
  (req as any).ctx = ctx;
  return ctx;
};

export type Context = SimpleRequestContext | ExpressContext;