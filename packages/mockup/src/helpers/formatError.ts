import { ApolloError } from 'apollo-server-express';
import { GraphQLError } from 'graphql';
import { InternalServerError } from '../errors';

export function formatError(error: Error) {
  let safeError: GraphQLError;
   if (error instanceof ApolloError) {
    safeError = error;
  } else if (error instanceof GraphQLError || error.constructor.name === 'GraphQLError') { // TODO Fix this comparison
     safeError = error as any;
   } else {
    safeError = new InternalServerError();
    // TODO USELESS UNLESS FIX (error instanceof ApolloError as false)
    if (error instanceof GraphQLError) {
      (safeError as any).path = error.path;
      (safeError as any).locations = error.locations;
      (safeError as any).extensions = error.extensions;
    }
    (safeError as any).originalError = error;
  }
  if (safeError instanceof InternalServerError
    || (safeError.extensions && safeError.extensions.code === 'INTERNAL_SERVER_ERROR')) {
   console.error(safeError.originalError, safeError.originalError.constructor);
  }
  return safeError;
}