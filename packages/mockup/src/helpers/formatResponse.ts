import { formatApolloErrors } from 'apollo-server-errors';
import { formatError } from './formatError';

export interface ApiResponse {
  data: any;
  errors?: Error[];
}

export function formatResponse(response: ApiResponse) {
  const { errors, ...rest } = response;
  return { ...rest, errors: errors ? formatApolloErrors(errors.map(formatError)) : errors };
}