import { addMockFunctionsToSchema } from 'graphql-tools';
import shortid from 'shortid';
import { GraphQLSchema } from 'graphql';
import {
  userMock,
  productMock,
  menuMock,
  descriptionMock,
} from '../mocks';

// Add mocks, modifies schema in place
export function addMock(schema: GraphQLSchema) {
  addMockFunctionsToSchema({
    schema,
    mocks: {
      DateTime: () => (new Date()).toISOString(),
      ObjectId: () => shortid(),
      Description: () => descriptionMock.sample(),
      Currency: () => ({ type: 'BRL', value: 12.50 }),
      LocationAddress: () => 'AVENIDA IPIRANGA, N° 6681 PRÉDIO 95A - TECNOPUC - Partenon, RS, 90619-900',
      PhoneNumber: () => '+555130212350',
      URL: () => 'http://example.com',
      Email: () => 'example@example.com',
      UnsignedInt: () => 1,
      GeoPoint: () => ({ lat: -30.0599111, lng: -51.20587 }),
      User: () => userMock.sample(),
      Product: () => productMock.sample(),
      Menu: () => menuMock.sample(),
    }
  });
}