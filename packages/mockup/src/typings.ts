export type Maybe<T> = T | null;
/** All built-in and custom scalars, mapped to their actual values */
export type Scalars = {
  ID: string,
  String: string,
  Boolean: boolean,
  Int: number,
  Float: number,
  /** The URL scalar represents a url string whose value conforms to the standard URL format as specified in RFC3986: https://www.ietf.org/rfc/rfc3986.txt. */
  URL: string,
  UnsignedInt: number,
  /** The `Currency` scalar represents an object of CurrencyType and Float amount, such as `{ type: "BRL", value: 29.99 }` */
  Currency: { type: CurrencyType, value: number },
  /** The `LocationAddress` scalar represent an address string accepted by Google Maps */
  LocationAddress: string,
  /** The `GeoPoint` scalar represents latitude and logintude object of Float values such as `{ "lat": -30.0586387, "lng": -51.1733803 }` */
  GeoPoint: { lat: number, lng: number },
  /** The `PhoneNumber` scalar represents phone number string whose value conforms to the standard E.164 format as specified in: https://en.wikipedia.org/wiki/E.164. Basically this is +17895551234. */
  PhoneNumber: string,
  /** The `Email` scalar type represents an email address string as specified by [RFC822](https://www.w3.org/Protocols/rfc822/). */
  Email: string,
  /** The `DateTime` scalar represents a date and time string following the ISO 8601 standard */
  DateTime: Date,
};








/** Status do passo do combo de produto */
export enum ComboProductStepStatus {
  /** Ativo */
  Active = 'ACTIVE',
  /** Removido */
  Removed = 'REMOVED'
}


/** Moeda */
export enum CurrencyType {
  /** Real Brasileiro */
  Brl = 'BRL'
}



/** Códigos de erro */
export enum ErrorCode {
  /** Erro interno do servidor */
  InternalServerError = 'INTERNAL_SERVER_ERROR',
  /** Erro de validação */
  ValidationError = 'VALIDATION_ERROR',
  /** Erro de autenticação requerida */
  AuthenticationRequiredError = 'AUTHENTICATION_REQUIRED_ERROR',
  /** Erro de entidade ou elemento não encontrado */
  EntityNotFoundError = 'ENTITY_NOT_FOUND_ERROR'
}



/** Marketplace */
export type Marketplace = {
   __typename?: 'Marketplace',
  /** ID global do marketplace */
  id: Scalars['ID'],
  /** Nome do marketplace */
  name: Scalars['String'],
};

/** Autenticação atual */
export type Me = {
   __typename?: 'Me',
  /** Usuário atual autenticado */
  userInfo: User,
  /** Marketplace associado a autenticação */
  marketplace?: Maybe<Marketplace>,
};

/** Menu (Cardápio) */
export type Menu = {
   __typename?: 'Menu',
  /** Id do menu */
  id: Scalars['ID'],
  /** Nome do menu */
  name: Scalars['String'],
  /** Id de lojas relacionadas */
  unitIds: Array<Scalars['ID']>,
  /** Lojas relacionada */
  units: Array<Unit>,
  /** Status do menu */
  status: MenuStatus,
  /** Id da faixa de preco relacionado */
  priceRangeId: Scalars['ID'],
  /** Faixa de preco relacionada ao menu */
  priceRange: PriceRange,
  /** Se os produtos do menu estão disponíveis para delivery */
  doesDelivery: Scalars['Boolean'],
  /** Se os produtos do menu estão disponíveis para delivery imediato */
  doesShortDelivery: Scalars['Boolean'],
  /** Se os produtos do menu estão disponíveis para retirada no balcão */
  dosTakeaway: Scalars['Boolean'],
  /** Se os produtos do menu estão disponíveis para consumo na loja */
  doesDineIn: Scalars['Boolean'],
  /** Data de publicação do menu */
  publishedAt: Scalars['DateTime'],
};

/** Alteração de Menu (Cardápio) */
export type MenuInput = {
  /** Nome do menu */
  name: Scalars['String'],
  /** Id da faixa de preco relacionado */
  priceRangeId: Scalars['ID'],
  /** Id das lojas relacionadas */
  unitIds: Array<Scalars['ID']>,
  /** Status do menu */
  status: MenuStatus,
  /** Se os produtos do menu estão disponíveis para delivery */
  doesDelivery: Scalars['Boolean'],
  /** Se os produtos do menu estão disponíveis para delivery imediato */
  doesShortDelivery: Scalars['Boolean'],
  /** Se os produtos do menu estão disponíveis para retirada no balcão */
  doesTakeaway: Scalars['Boolean'],
  /** Se os produtos do menu estão disponíveis para consumo na loja */
  doesDineIn: Scalars['Boolean'],
  /** Id validação do cliente para a mutação */
  clientMutationId?: Maybe<Scalars['String']>,
};

/** Payload do Menu (Cardápio) */
export type MenuPayload = {
   __typename?: 'MenuPayload',
  /** Menu (Cardápio) */
  menu: Menu,
  /** Id de mutação informado pelo cliente */
  clientMutationId?: Maybe<Scalars['String']>,
};

/** Status do menu */
export enum MenuStatus {
  /** Pendente */
  Pending = 'PENDING',
  /** Publicado */
  Published = 'PUBLISHED',
  /** Removido */
  Removed = 'REMOVED'
}

/** Meta produto - é um rascunho de produto que precisa ser curado para se tornar um produto completo */
export type MetaProduct = {
   __typename?: 'MetaProduct',
  /** Id externo de integração ou controle */
  externalId: Scalars['String'],
  /** Id do marketplace relacionado */
  marketplaceId: Scalars['Int'],
  /** Objeto marketplace relacionado */
  marketplace: Marketplace,
  /** Nome do meta produto */
  name: Scalars['String'],
  /** Status do meta produto */
  status: MetaProductStatus,
  /** Tipo do meta produto */
  type: MetaProductStatus,
};

/** Alteração de meta produto */
export type MetaProductInput = {
  /** Id externo de integração ou controle */
  externalId: Scalars['String'],
  /** Nome do meta produto */
  name: Scalars['String'],
  /** Status do meta produto */
  status: MetaProductStatus,
  /** Tipo do meta produto */
  type: MetaProductType,
  /** Id validação do cliente para a mutação */
  clientMutationId?: Maybe<Scalars['String']>,
};

/** Payload da mutação do meta produto */
export type MetaProductPayload = {
   __typename?: 'MetaProductPayload',
  /** Meta produto */
  productMeta: MetaProduct,
  /** Id de mutação informado pelo cliente */
  clientMutationId?: Maybe<Scalars['String']>,
};

/** Status do meta produto */
export enum MetaProductStatus {
  /** Ativo */
  Active = 'ACTIVE',
  /** Inativo */
  Inactive = 'INACTIVE'
}

/** Tipo de meta produto */
export enum MetaProductType {
  /** Adicional */
  Additional = 'ADDITIONAL',
  /** Ingrediente */
  Igredient = 'IGREDIENT',
  /** Combo */
  Combo = 'COMBO',
  /** Produto padrão */
  Default = 'DEFAULT'
}

export type Mutation = {
   __typename?: 'Mutation',
  /** Cria um meta produto */
  createMetaProduct: MetaProductPayload,
  /** Atualiza um meta produto */
  updateMetaProduct: MetaProductPayload,
  /** Cria uma loja */
  createUnit: UnitPayload,
  /** Atualiza uma loja */
  updateUnit: UnitPayload,
  /** Cria um produto */
  createProduct: ProductPayload,
  /** Atualiza um produto */
  updateProduct: ProductPayload,
  /** Cria uma categoria de produto */
  createProductCategory: ProductCategoryPayload,
  /** Atualiza uma categoria de produto */
  updateCategoryProduct: ProductCategoryPayload,
  /** Cria uma etiqueta de produto */
  createProductTag: ProductTagPayload,
  /** Atualiza uma etiqueta de produto */
  updateProductTag: ProductTagPayload,
  /** Cria um passo de combo de produto */
  createProductComboStep: ProductComboStepPayload,
  /** Atualiza um passo de combo de produto */
  updateProductComboStep: ProductComboStepPayload,
  /** Cria uma opçao de variação de produto */
  createProductVariationOption: ProductVariationOptionPayload,
  /** Atualiza uma opçao de variação de produto */
  updateProductVariationOption: ProductVariationOptionPayload,
  /** Cria uma variação de produto */
  createProductVariation: ProductVariationPayload,
  /** Atualiza variação de produto */
  updateProductVariation: ProductVariationPayload,
  /** Cria uma faixa de preco */
  createPriceRange: PriceRangePayload,
  /** Atualiza uma faixa de preco */
  updatePriceRange: PriceRangePayload,
  /** Cria uma relação de faixa de preço e produto */
  createPriceRangeRelationship: PriceRangeRelationshipPayload,
  /** Atualiza uma relação de faixa de preço e produto */
  updatePriceRangeRelationship: PriceRangeRelationshipPayload,
  /** Cria um menu */
  createMenu: MenuPayload,
  /** Atualiza um menu */
  updateMenu: MenuPayload,
};


export type MutationCreateMetaProductArgs = {
  input: MetaProductInput
};


export type MutationUpdateMetaProductArgs = {
  id: Scalars['ID'],
  input: MetaProductInput
};


export type MutationCreateUnitArgs = {
  input: UnitInput
};


export type MutationUpdateUnitArgs = {
  id: Scalars['ID'],
  input: UnitInput
};


export type MutationCreateProductArgs = {
  input: ProductInput
};


export type MutationUpdateProductArgs = {
  id: Scalars['ID'],
  input: ProductInput
};


export type MutationCreateProductCategoryArgs = {
  input: ProductCategoryInput
};


export type MutationUpdateCategoryProductArgs = {
  id: Scalars['ID'],
  input: ProductCategoryInput
};


export type MutationCreateProductTagArgs = {
  input: ProductTagInput
};


export type MutationUpdateProductTagArgs = {
  id: Scalars['ID'],
  input: ProductTagInput
};


export type MutationCreateProductComboStepArgs = {
  input: ProductComboStepInput
};


export type MutationUpdateProductComboStepArgs = {
  id: Scalars['ID'],
  input: ProductComboStepInput
};


export type MutationCreateProductVariationOptionArgs = {
  input: ProductVariationOptionInput
};


export type MutationUpdateProductVariationOptionArgs = {
  id: Scalars['ID'],
  input: ProductVariationOptionInput
};


export type MutationCreateProductVariationArgs = {
  input: ProductVariationInput
};


export type MutationUpdateProductVariationArgs = {
  id: Scalars['ID'],
  input: ProductVariationInput
};


export type MutationCreatePriceRangeArgs = {
  input: PriceRangeInput
};


export type MutationUpdatePriceRangeArgs = {
  id: Scalars['ID'],
  input: PriceRangeInput
};


export type MutationCreatePriceRangeRelationshipArgs = {
  input: PriceRangeRelationshipInput
};


export type MutationUpdatePriceRangeRelationshipArgs = {
  id: Scalars['ID'],
  input: PriceRangeRelationshipInput
};


export type MutationCreateMenuArgs = {
  input: MenuInput
};


export type MutationUpdateMenuArgs = {
  id: Scalars['ID'],
  input: MenuInput
};

export type Node = {
  id: Scalars['ID'],
};

/** Objetos implementando a interface Node */
export enum NodeType {
  User = 'User',
  Marketplace = 'Marketplace',
  Product = 'Product',
  ProductVariation = 'ProductVariation',
  ProductVariationOption = 'ProductVariationOption',
  Menu = 'Menu',
  PriceRange = 'PriceRange',
  ProductTag = 'ProductTag',
  ProductCategory = 'ProductCategory',
  Store = 'Store',
  ComboProductStep = 'ComboProductStep'
}


/** Faixa de preço */
export type PriceRange = {
   __typename?: 'PriceRange',
  /** Id da faixa de preço */
  id: Scalars['ID'],
  /** Nome da faixa de preço */
  name: Scalars['String'],
};

/** Alteração da faixa de preço */
export type PriceRangeInput = {
  /** Nome da faixa de preço */
  name: Scalars['String'],
  /** Id validação do cliente para a mutação */
  clientMutationId?: Maybe<Scalars['String']>,
};

/** Payload da mutação da faixa de preço */
export type PriceRangePayload = {
   __typename?: 'PriceRangePayload',
  /** Faixa de preço */
  priceRange: PriceRange,
  /** Id de mutação informado pelo cliente */
  clientMutationId?: Maybe<Scalars['String']>,
};

/** Relação de faixa de preço e produto */
export type PriceRangeRelationship = {
   __typename?: 'PriceRangeRelationship',
  /** Id da faixa de preco relacionada */
  priceRangeId: Scalars['ID'],
  /** Id do produto relacionado */
  productId: Scalars['ID'],
  /** Preço do produto relacionado */
  price: Scalars['Currency'],
};

/** Alteração da relação de faixa de preço e produto */
export type PriceRangeRelationshipInput = {
  /** Id da faixa de preco relacionada */
  priceRangeId: Scalars['ID'],
  /** Id do produto relacionado */
  productId: Scalars['ID'],
  /** Preço do produto relacionado */
  price: Scalars['Currency'],
  /** Id validação do cliente para a mutação */
  clientMutationId?: Maybe<Scalars['String']>,
};

/** Payload da mutação da relação de faixa de preço e produto */
export type PriceRangeRelationshipPayload = {
   __typename?: 'PriceRangeRelationshipPayload',
  /** Relação de faixa de preço e produto */
  priceRangeRalationship: PriceRangeRelationship,
  /** Id de mutação informado pelo cliente */
  clientMutationId?: Maybe<Scalars['String']>,
};

/** Produto */
export type Product = Node & {
   __typename?: 'Product',
  /** ID do produto */
  id: Scalars['ID'],
  /** Nome do produto */
  name: Scalars['String'],
  /** Subtítulo do produto */
  subheading?: Maybe<Scalars['String']>,
  /** Descrição do produto */
  description?: Maybe<Scalars['String']>,
  /** Imagem do produto */
  imageUrl?: Maybe<Scalars['URL']>,
  /** Status do produto */
  status: ProductStatus,
  /** Se o produto pode ser retirado no balcão */
  doesTakeaway: Scalars['Boolean'],
  /** Se o produto pode ser enviado por delivery */
  doesDelivery: Scalars['Boolean'],
  /** Se o produto pode ser consumido no local */
  doesDiveIn: Scalars['Boolean'],
  /** Relação das faixas de preço por id */
  priceRangeIds: Array<Scalars['ID']>,
  /** Tipo do produto */
  productType: ProductType,
  /** Tags do produto */
  productTagIds?: Maybe<Array<Scalars['ID']>>,
  /** Tags do produto */
  productTags?: Maybe<Array<ProductTag>>,
  /** Se o produto é destacado */
  isFeatured: Scalars['Boolean'],
  /** Ids de passos de combo relacionados (quando o produto for combo) */
  comboProductStepIds?: Maybe<Array<Scalars['ID']>>,
  /** Passos de combo relacionados (quando o produto for combo) */
  comboProductSteps?: Maybe<Array<ProductComboStep>>,
  /** Id das categorias do produto */
  categoryIds?: Maybe<Array<Scalars['ID']>>,
  /** Categorias do produto */
  categories?: Maybe<Array<ProductCategory>>,
  /** Ids de variações do produto */
  variationIds?: Maybe<Array<Scalars['ID']>>,
  /** Variações do produto */
  variations?: Maybe<Array<ProductVariation>>,
  /** Se o produto é alimentado através de uma integração (Ex. PDV) */
  isIntegrationProduct: Scalars['Boolean'],
};

/** Categoria de produto */
export type ProductCategory = {
   __typename?: 'ProductCategory',
  /** Id da categoria do produto */
  id: Scalars['ID'],
  /** Título da categoria do produto */
  title: Scalars['String'],
  /** Ordem da categoria do produto */
  sortOrder: Scalars['UnsignedInt'],
  /** URL da imagem associada a categoria do produto */
  imageUrl: Scalars['URL'],
  /** Id externo de integração ou controle */
  externalId?: Maybe<Scalars['String']>,
};

/** Alteraçao de categoria de produto */
export type ProductCategoryInput = {
  /** Título da categoria do produto */
  title: Scalars['String'],
  /** Ordem da categoria do produto */
  sortOrder?: Maybe<Scalars['UnsignedInt']>,
  /** URL da imagem associada a categoria do produto */
  imageUrl?: Maybe<Scalars['URL']>,
  /** Id externo de integração ou controle */
  externalId?: Maybe<Scalars['String']>,
  /** Id validação do cliente para a mutação */
  clientMutationId?: Maybe<Scalars['String']>,
};

/** Payload da mutação da categoria de produto */
export type ProductCategoryPayload = {
   __typename?: 'ProductCategoryPayload',
  /** Categoria de produto */
  category: ProductCategory,
  /** Id de mutação informado pelo cliente */
  clientMutationId?: Maybe<Scalars['String']>,
};

/** Passo do combo de produto */
export type ProductComboStep = {
   __typename?: 'ProductComboStep',
  /** Id do passo do combo de produto */
  id: Scalars['ID'],
  /** Nome do passo do combo de produto */
  name: Scalars['String'],
  /** Id do produto relacionado ao passo do combo */
  productId: Scalars['ID'],
  /** Status do passo do combo de produto */
  status: ComboProductStepStatus,
  /** Ordem de listagem do passo do combo de produto */
  sortOrder: Scalars['UnsignedInt'],
  /** Id externo de integração ou controle */
  externalId?: Maybe<Scalars['String']>,
};

/** Alteracão de passo do combo de produto */
export type ProductComboStepInput = {
  /** Nome do passo do combo de produto */
  name: Scalars['String'],
  /** Id do produto relacionado ao passo do combo */
  productId: Scalars['ID'],
  /** Status do passo do combo de produto */
  status: ComboProductStepStatus,
  /** Ordem de listagem do passo do combo de produto */
  sortOrder?: Maybe<Scalars['UnsignedInt']>,
  /** Id externo da integraçao */
  externalId?: Maybe<Scalars['String']>,
  /** Id validação do cliente para a mutação */
  clientMutationId?: Maybe<Scalars['String']>,
};

/** Payload da mutação de passo do combo de produto */
export type ProductComboStepPayload = {
   __typename?: 'ProductComboStepPayload',
  /** Passo do combo de produto */
  comboProductStep: ProductComboStep,
  /** Id de mutação informado pelo cliente */
  clientMutationId?: Maybe<Scalars['String']>,
};

/** Alteração de produto */
export type ProductInput = {
  /** Nome do produto */
  name: Scalars['String'],
  /** Descrição do produto */
  description?: Maybe<Scalars['String']>,
  /** Imagem do produto */
  imageUrl?: Maybe<Scalars['URL']>,
  /** Status do produto */
  status: ProductStatus,
  /** Se o produto pode ser retirado no balcão */
  doesTakeaway: Scalars['Boolean'],
  /** Se o produto pode ser enviado por delivery */
  doesDelivery: Scalars['Boolean'],
  /** Se o produto pode ser consumido no local */
  doesDiveIn: Scalars['Boolean'],
  /** Relação das faixas de preço por id */
  priceRangeIds: Array<Scalars['ID']>,
  /** Tipo do produto */
  type?: Maybe<ProductType>,
  /** Tags do produto */
  productTagIds?: Maybe<Array<Scalars['ID']>>,
  /** Se o produto é destacado */
  isFeatured: Scalars['Boolean'],
  /** Passos de combo relacionados (quando o produto for combo) */
  productComboStepIds?: Maybe<Array<Scalars['ID']>>,
  /** Categorias do produto */
  productCategoryIds?: Maybe<Array<Scalars['ID']>>,
  /** Variações globais do produto */
  productVariationIds?: Maybe<Array<Scalars['ID']>>,
  /** Id validação do cliente para a mutação */
  clientMutationId?: Maybe<Scalars['String']>,
};

/** Payload da mutação do produto */
export type ProductPayload = {
   __typename?: 'ProductPayload',
  /** Produto */
  product: Product,
  /** Id de mutação informado pelo cliente */
  clientMutationId?: Maybe<Scalars['String']>,
};

/** Status do produto */
export enum ProductStatus {
  /** Ativo */
  Active = 'ACTIVE',
  /** Inativo */
  Inactive = 'INACTIVE',
  /** Esgotado / Em falta */
  SoldOff = 'SOLD_OFF',
  /** Removido */
  Removed = 'REMOVED'
}

export type ProductTag = {
   __typename?: 'ProductTag',
  /** Id da tag de produto */
  id: Scalars['ID'],
  /** Título da tag de produto */
  title: Scalars['String'],
};

/** Alteraçao da etiqueta de produto */
export type ProductTagInput = {
  /** Título da tag de produto */
  title: Scalars['String'],
  /** Id validação do cliente para a mutação */
  clientMutationId?: Maybe<Scalars['String']>,
};

/** Payload da mutação da etiqueta de produto */
export type ProductTagPayload = {
   __typename?: 'ProductTagPayload',
  /** Etiqueta de produto */
  productTag: ProductTag,
  /** Id de mutação informado pelo cliente */
  clientMutationId?: Maybe<Scalars['String']>,
};

/** Tipo de produto */
export enum ProductType {
  /** Combo */
  Combo = 'COMBO',
  /** Produto padrão */
  Default = 'DEFAULT'
}

/** Variação do produto */
export type ProductVariation = {
   __typename?: 'ProductVariation',
  /** Id da variação */
  id: Scalars['ID'],
  /** Nome da variação */
  name: Scalars['String'],
  /** Título da variação */
  title: Scalars['String'],
  /** Tipo da variação */
  type: ProductVariationType,
  /** Tipo de opção de variação */
  optionsType: ProductVariationOptionType,
  /** Mínimo de inclusões dessa variação ao produto */
  min: Scalars['UnsignedInt'],
  /** Máximo de inclusões dessa variação ao produto */
  max: Scalars['UnsignedInt'],
  /** Valor de ordenação da variação */
  sortOrder: Scalars['UnsignedInt'],
  /** Status da variação do produto */
  status: ProductVariationStatus,
  /** Tipo de preço da variação do produto */
  priceType: ProductVariationPriceType,
  /** Id do marketplace relacionado */
  marketplaceId: Scalars['ID'],
  /** Marketplace relacionado */
  marketplace: Marketplace,
  /** Opções de variação de produto relacionadas */
  options?: Maybe<Array<ProductVariationOption>>,
};

/** Alteração da variação do produto */
export type ProductVariationInput = {
  /** Nome da variação */
  name: Scalars['String'],
  /** Título da variação */
  title: Scalars['String'],
  /** Tipo da variação */
  productVariationTypeId: Scalars['ID'],
  /** Tipo de opção de variação */
  optionsType: ProductVariationOptionType,
  /** Mínimo de inclusões dessa variação ao produto */
  min?: Maybe<Scalars['UnsignedInt']>,
  /** Máximo de inclusões dessa variação ao produto */
  max?: Maybe<Scalars['UnsignedInt']>,
  /** Valor de ordenação da variação */
  sortOrder?: Maybe<Scalars['UnsignedInt']>,
  /** Status da variação do produto */
  status: ProductVariationStatus,
  /** Tipo de preço da variação do produto */
  priceType: ProductVariationPriceType,
  /** Ids das opções de variação de produto relacionadas */
  productVariationOptionIds?: Maybe<Array<Scalars['ID']>>,
  /** Id validação do cliente para a mutação */
  clientMutationId?: Maybe<Scalars['String']>,
};

export type ProductVariationOption = {
   __typename?: 'ProductVariationOption',
  /** Id da opção de variação do produto */
  id: Scalars['ID'],
  /** Nome da opção de variação do produto */
  name: Scalars['String'],
  /** Id externo da opção de variação do produto */
  externalId?: Maybe<Scalars['String']>,
  /** Descrição da opção de variação do produto */
  description: Scalars['String'],
  /** Preço original da opção de variação do produto */
  price: Scalars['Currency'],
  /** Máximo de inclusões da opção de variação do produto */
  max: Scalars['UnsignedInt'],
  /** URL da imagem da opção de variação do produto */
  imageUrl: Scalars['URL'],
  /** SKU (Stock keeping unit) da opção de variação do produto */
  sku?: Maybe<Scalars['String']>,
  /** Ordem de listagem da opção de variação do produto */
  sortOrder: Scalars['UnsignedInt'],
  /** Status da opção de variação do produto */
  status: ProductVariationOptionStatus,
};

/** Alteraçao de opçao de variação de produto */
export type ProductVariationOptionInput = {
  /** Nome da opção de variação do produto */
  name: Scalars['String'],
  /** Id externo da opção de variação do produto */
  externalId?: Maybe<Scalars['String']>,
  /** Descrição da opção de variação do produto */
  description: Scalars['String'],
  /** Preço original da opção de variação do produto */
  price: Scalars['Currency'],
  /** Máximo de inclusões da opção de variação do produto */
  max?: Maybe<Scalars['UnsignedInt']>,
  /** Imagem da opção da variação */
  imageUrl?: Maybe<Scalars['URL']>,
  /** SKU (Stock keeping unit) da opção de variação do produto */
  sku?: Maybe<Scalars['String']>,
  /** Ordem de listagem da opção de variação do produto */
  sortOrder?: Maybe<Scalars['UnsignedInt']>,
  /** Status da opção de variação do produto */
  status: ProductVariationOptionStatus,
  /** Id validação do cliente para a mutação */
  clientMutationId?: Maybe<Scalars['String']>,
};

/** Payload da mutação de opçao de variação de produto */
export type ProductVariationOptionPayload = {
   __typename?: 'ProductVariationOptionPayload',
  /** Opçao de variação de produto */
  productVariationOption: ProductVariationOption,
  /** Id de mutação informado pelo cliente */
  clientMutationId?: Maybe<Scalars['String']>,
};

/** Status de opção de variação do meta produto */
export enum ProductVariationOptionStatus {
  /** Ativo */
  Active = 'ACTIVE',
  /** Inativo */
  Inactive = 'INACTIVE'
}

/** Tipo de variação de opção de produto */
export enum ProductVariationOptionType {
  /** Inativo */
  Inactive = 'INACTIVE',
  /** Ativo */
  Active = 'ACTIVE'
}

/** Payload da mutação da variação de produto */
export type ProductVariationPayload = {
   __typename?: 'ProductVariationPayload',
  /** Variaçao do produto */
  productVariation: ProductVariation,
  /** Id de mutação informado pelo cliente */
  clientMutationId?: Maybe<Scalars['String']>,
};

/** Tipo do cálculo de preço da variação do produto */
export enum ProductVariationPriceType {
  /** A preço da variação soma com o valor total do produto */
  SomaTotal = 'SOMA_TOTAL',
  /** A preço da variação soma com o valor médio da variação do produto */
  PrecoMedio = 'PRECO_MEDIO'
}

/** Status da variação do produto */
export enum ProductVariationStatus {
  /** Ativo */
  Active = 'ACTIVE',
  /** Inativo */
  Inactive = 'INACTIVE'
}

/** Tipo de variação de produto */
export type ProductVariationType = {
   __typename?: 'ProductVariationType',
  /** Id do tipo de variação de produto */
  id: Scalars['ID'],
};

export type Query = {
   __typename?: 'Query',
  me?: Maybe<Me>,
  errorCodes: Array<ErrorCode>,
  node: Node,
  marketplace: Marketplace,
  product: Product,
  productCategory: ProductCategory,
  productTag: ProductTag,
  metaProduct: MetaProduct,
  productVariation: ProductVariation,
  productVariationType: ProductVariationType,
  productVariationOption: ProductVariationOption,
  productComboStep: ProductComboStep,
  menu: Menu,
  productType: ProductType,
  priceRange: PriceRange,
  priceRangeRelationship: PriceRangeRelationship,
  unit: Unit,
  user: User,
  userRole: UserRole,
};


export type QueryNodeArgs = {
  id: Scalars['ID']
};


export type QueryMarketplaceArgs = {
  id: Scalars['ID']
};


export type QueryProductArgs = {
  id: Scalars['ID']
};


export type QueryProductCategoryArgs = {
  id: Scalars['ID']
};


export type QueryProductTagArgs = {
  id: Scalars['ID']
};


export type QueryMetaProductArgs = {
  id: Scalars['ID']
};


export type QueryProductVariationArgs = {
  id: Scalars['ID']
};


export type QueryProductVariationTypeArgs = {
  id: Scalars['ID']
};


export type QueryProductVariationOptionArgs = {
  id: Scalars['ID']
};


export type QueryProductComboStepArgs = {
  id: Scalars['ID']
};


export type QueryMenuArgs = {
  id: Scalars['ID']
};


export type QueryProductTypeArgs = {
  id: Scalars['ID']
};


export type QueryPriceRangeArgs = {
  id: Scalars['ID']
};


export type QueryPriceRangeRelationshipArgs = {
  id: Scalars['ID']
};


export type QueryUnitArgs = {
  id: Scalars['ID']
};


export type QueryUserArgs = {
  id: Scalars['ID']
};


export type QueryUserRoleArgs = {
  id: Scalars['ID']
};

export type Schema = {
   __typename?: 'Schema',
  query?: Maybe<Query>,
  mutation?: Maybe<Mutation>,
};

/** Loja */
export type Unit = {
   __typename?: 'Unit',
  /** Id da loja */
  id: Scalars['ID'],
  /** Nome da loja */
  name: Scalars['String'],
  /** Descrição da loja */
  description?: Maybe<Scalars['String']>,
  /** Endereço da loja */
  locationAddress?: Maybe<Scalars['LocationAddress']>,
  /** Latitude e longitude da loja */
  latLon?: Maybe<Scalars['GeoPoint']>,
  /** Telefone fixo de contato da loja */
  phoneNumber?: Maybe<Scalars['PhoneNumber']>,
  /** Telefone móvel de contato da loja */
  mobileNumber?: Maybe<Scalars['PhoneNumber']>,
  /** Site da loja */
  siteUrl?: Maybe<Scalars['URL']>,
  /** Email da loja */
  email?: Maybe<Scalars['Email']>,
  /** Facebook da loja */
  facebookUrl?: Maybe<Scalars['URL']>,
  /** Twitter da loja */
  twitterUrl?: Maybe<Scalars['URL']>,
  /** Instagram da loja */
  instagramUrl?: Maybe<Scalars['URL']>,
};

/** Alteração de loja */
export type UnitInput = {
  /** Nome da loja */
  name: Scalars['String'],
  /** Descrição da loja */
  description?: Maybe<Scalars['String']>,
  /** Endereço da loja */
  locationAddress?: Maybe<Scalars['LocationAddress']>,
  /** Latitude e longitude da loja */
  latLon?: Maybe<Scalars['GeoPoint']>,
  /** Telefone fixo de contato da loja */
  phoneNumber?: Maybe<Scalars['PhoneNumber']>,
  /** Telefone móvel de contato da loja */
  mobileNumber?: Maybe<Scalars['PhoneNumber']>,
  /** Site da loja */
  siteUrl?: Maybe<Scalars['URL']>,
  /** Email da loja */
  email?: Maybe<Scalars['Email']>,
  /** Facebook da loja */
  facebookUrl?: Maybe<Scalars['URL']>,
  /** Twitter da loja */
  twitterUrl?: Maybe<Scalars['URL']>,
  /** Instagram da loja */
  instagramUrl?: Maybe<Scalars['URL']>,
  /** Id validação do cliente para a mutação */
  clientMutationId?: Maybe<Scalars['String']>,
};

/** Payload da mutação da loja */
export type UnitPayload = {
   __typename?: 'UnitPayload',
  /** Loja */
  unit: Unit,
  /** Id de mutação informado pelo cliente */
  clientMutationId?: Maybe<Scalars['String']>,
};



export type User = {
   __typename?: 'User',
  /** Id da 4ALL */
  _id4All: Scalars['String'],
  /** Id do usuário */
  id: Scalars['ID'],
  /** Tipo do usuário */
  type: UserType,
  /** Nome completo do usuário */
  fullName: Scalars['String'],
  /** E-mail do usuário */
  emailAddress: Scalars['String'],
  /** Documento do usuário (Ex. CPNJ or CPF) */
  documentNumber: Scalars['String'],
  /** Telefone de contato do usuário */
  phoneNumber: Scalars['String'],
  /** Papel atribuído ao usuário */
  role: UserRole,
};

/** Papel do usuário */
export type UserRole = {
   __typename?: 'UserRole',
  /** Id do papel do usuário */
  id: Scalars['ID'],
  /** Nome do papel do usuário */
  name: Scalars['String'],
};

/** Tipo de usuário */
export enum UserType {
  /** Usuário */
  User = 'USER',
  /** Conta */
  Account = 'ACCOUNT',
  /** Micro Conta */
  MicroAccount = 'MICRO_ACCOUNT'
}
