import { ApolloError } from 'apollo-server-express';

export class InvalidTokenError extends ApolloError {
  constructor() {
    super(`Token is invalid or has expired`, 'INVALID_TOKEN_ERROR');
  }

}