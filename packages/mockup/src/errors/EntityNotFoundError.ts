import { ApolloError } from 'apollo-server-express';

export class EntityNotFoundError extends ApolloError {
  constructor(entityName: string, key: any) {
    super(`Entity Not Found`, 'ENTITY_NOT_FOUND_ERROR', {
      entityName,
      key,
    });
  }

}