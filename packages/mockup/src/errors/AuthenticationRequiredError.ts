import { ApolloError } from 'apollo-server-express';

export class AuthenticationRequiredError extends ApolloError {
  constructor() {
    super(`Authentication required`, 'AUTHENTICATION_REQUIRED_ERROR');
  }

}