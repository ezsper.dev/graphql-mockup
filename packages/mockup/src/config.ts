export const config = {
  port: process.env.GRAPHQL_PORT != null
    ? parseInt(process.env.GRAPHQL_PORT)
    : process.env.NODE_ENV !== 'production' ? 7080 : 80,
};