import sample from 'lodash/sample';
import sampleSize from 'lodash/sampleSize';

export class Mocks<T> {

    constructor(readonly items: T[]) {}

    eq(index: number) {
        return this.items[index];
    }

    sample() {
        return sample(this.items);
    }

    samples(length: number) {
        return sampleSize(this.items, length);
    }

    toArray() {
        return this.items.slice(0);
    }

    *[Symbol.iterator]() {
        for (const item of this.items) {
            yield item;
        }
    }

}