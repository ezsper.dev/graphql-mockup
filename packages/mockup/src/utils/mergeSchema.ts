import { resolve as resolvePath } from 'path';
import fs from 'fs';
import Glob from 'glob';

export function mergeSchema(cwd: string, pattern = './**/*.graphql') {
  const files = Glob.sync(pattern, {
    cwd,
  });

  if (!files) {
    throw new Error(`No schema files were found`);
  }

  let typeDefs = '';

  for (const file of files) {
    typeDefs += `${fs.readFileSync(resolvePath(cwd, file)).toString()}\n\n`;
  }

  return typeDefs;
}